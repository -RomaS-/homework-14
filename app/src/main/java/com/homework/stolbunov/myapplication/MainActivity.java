package com.homework.stolbunov.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RelativeLayout layout = findViewById(R.id.relative_layout);

        RelativeLayout.LayoutParams paramsImageView = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, R.dimen.height_image_view);
        paramsImageView.addRule(RelativeLayout.BELOW, R.id.image_view_xml);

        ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.wolves);
        imageView.setLayoutParams(paramsImageView);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        RelativeLayout.LayoutParams paramsTextView = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsTextView.addRule(RelativeLayout.CENTER_HORIZONTAL);
        paramsTextView.addRule(RelativeLayout.BELOW, R.id.image_view_xml);

        TextView textView = new TextView(this);
        textView.setText(R.string.img_name_wolves);

        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
        textView.setTextColor(getResources().getColor(R.color.colorTextImage));
        textView.setLayoutParams(paramsTextView);

        layout.addView(imageView);
        layout.addView(textView);
    }
}
